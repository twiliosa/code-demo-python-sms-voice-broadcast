import os
import urllib
import requests
import config
import json
from twilio import twiml, TwilioRestException
from twilio.rest import TwilioRestClient

from firebase import firebase

from flask import Flask, redirect, url_for
from flask import request, json

app = Flask(__name__, static_url_path='')

#From Numbers:
FROM_NUMBER_UK = '+441323702021'
FROM_NUMBER_US_573 = '+15737725629'
FROM_NUMBER_US_978 = '+19787672498'
FROM_NUMBER_US_469 = '+14695184100'

from_number_voice = FROM_NUMBER_US_469
from_number_sms = FROM_NUMBER_US_469
voice_language = 'en-US'


# Dictionary of Names
recipients = {'+16507439658': 'Ameer',
              '+14159387868': 'Ameer-Google'}

# Dictionary of options
options_menu_list = {'BAL': 'Your current balance:\n$100\nHave a great day!',
            'DUE': 'Your payment is due on:\nApril 15, 2014\nHave a great day!',
            'STATUS': 'Your current loan approval status:\nUnder Final Review\nHave a great day!'
            }

def get_options_menu(option):
    try:
        option_menu = options_menu_list[option]
    except KeyError:
        option_menu = "Not Available"
    return option_menu


def update_participant_firebase_status(customer_name, notification_type, phone_number, status):
    fb = firebase.FirebaseApplication('https://ab-alerts-twilio.firebaseio.com', None)
    participant_path = '/' + customer_name + '/participants/' + notification_type + '/' + urllib.quote_plus(phone_number)
    print participant_path
    fb.patch(participant_path, {'status':status})


# Send outbound Alerts
# @app.route('/send_outbound_alerts', methods=['POST', 'GET'])
def send_outbound_alerts(customer_name, message_body):
    # f = request.json
    # message = f['body']
    message = message_body
    print 'Message Text: ' + message
    # Get Twilio client object
    client = TwilioRestClient(config.ACCOUNT_SID, config.AUTH_TOKEN)
    sms_participants = {}
    voice_participants = {}
    fb = firebase.FirebaseApplication('https://ab-alerts-twilio.firebaseio.com', None)
    fb_path = '/' + customer_name + '/participants'
    sms_participants = fb.get(fb_path, 'sms')
    voice_participants = fb.get(fb_path, 'voice')
    participant = {}
    # Process SMS Alerts outbound
    try:
        for key in sms_participants.keys():
            participant['phone'] = key
            print key
            for k, v in sms_participants[key].iteritems():
                participant[k] = v
                print k + ' => ' + v
            # Now send the SMS
            message_body = 'Hello ' + participant['name'] + ':\n' + message
            print 'Outbound Message Body: ' + message_body
            params = {'customer': customer_name}
            sms_status_callback_url = 'https://' + request.host + '/sms_callback?' + urllib.urlencode(params)
            sms = client.messages.create(to=participant['phone'], from_=from_number_sms, body =message_body, status_callback =sms_status_callback_url)
            print "Message Sid: " + sms.sid
            participant_path = fb_path + '/sms/' + urllib.quote_plus(participant['phone'])
            print participant_path
            update_participant_firebase_status(customer_name, 'sms', participant['phone'], sms.status)
            # fb.patch(participant_path, {'status':sms.status})
    except:
        pass
    participant = {}
    # Process Voice Alerts outbound
    try:
        for key in voice_participants.keys():
            participant['phone'] = key
            for k, v in voice_participants[key].iteritems():
                participant[k] = v
                print k + ' => ' + v
            # Now send the Voice call
            message_body = 'Hello ' + participant['name'] + ':\n' + message
            print 'Outbound Message Body: ' + message_body
            params = {'customer': customer_name,'voicemessage' : message_body}
            call_url = 'https://' + request.host + '/outbound_ivr?' + urllib.urlencode(params)
            # print 'Call URL: ' + call_url
            voice_status_callback_url = 'https://' + request.host + '/voice_callback?' + urllib.urlencode(params)
            call = client.calls.create(from_=from_number_voice, to=participant['phone'], url=call_url, status_callback =voice_status_callback_url)
            update_participant_firebase_status(customer_name, 'voice', participant['phone'], 'calling')
    except:
        pass
    return 'OK'


# Twilio SMS Sent callback
@app.route('/sms_callback', methods=['POST', 'GET'])
def sms_callback():
    to_number = request.values.get('To')
    status = request.values.get('MessageStatus', 'NA')
    customer_name = request.values.get('customer')
    print 'Message To: ' + to_number
    print 'Message Status: ' + status
    print 'Customer Name: ' + customer_name
    # Update status on Firebase
    update_participant_firebase_status(customer_name, 'sms', to_number, status)
    resp = twiml.Response()
    return str(resp)


# Twilio Voice Sent callback
@app.route('/voice_callback', methods=['POST', 'GET'])
def voice_callback():
    to_number = request.values.get('To')
    status = request.values.get('CallStatus', 'NA')
    customer_name = request.values.get('customer')
    print 'Call To: ' + to_number
    print 'Call Status: ' + status
    print 'Customer Name: ' + customer_name
    # Update status on Firebase
    update_participant_firebase_status(customer_name, 'voice', to_number, status)
    resp = twiml.Response()
    return str(resp)


# Broadcast repair info to customers
@app.route('/outbound_ivr', methods=['POST', 'GET'])
def outbound_ivr():
    to_number = request.values.get('To')
    voice_message = request.values.get('voicemessage', 'Hello')
    customer_name = request.values.get('customer')
    print voice_message
    resp = twiml.Response()
    resp.play('https://' + request.host + '/media/greeting.mp3')
    params = {'customer': customer_name, 'voicemessage' : voice_message}
    gather_action_url = 'https://' + request.host + '/outbound_ivr_process_gather?' + urllib.urlencode(params)
    # print gather_action_url
    with resp.gather(numDigits=1, action=gather_action_url, method="POST") as g:
        g.say('Press 1 to listen to your message', voice=voice_language)
        g.say('Press 2 to speak to an agent', voice=voice_language)
        g.say('Press 0 to hangup', voice=voice_language)
    update_participant_firebase_status(customer_name, 'voice', to_number, 'answered')
    # Reply back with TwiML
    print 'Returned TwiML: ' + str(resp)
    return str(resp)

@app.route('/outbound_ivr_process_gather', methods=['POST', 'GET'])
def outbound_ivr_process_gather():
    voice_message = request.values.get('voicemessage', 'Hello')
    digits = request.values.get('Digits', '')
    customer_name = request.values.get('customer')
    print 'Digit Pressed: ' + digits
    resp = twiml.Response()
    if digits == '1':
        resp.say(voice_message, voice=voice_language)
    elif digits == '2':
        resp.say('Connecting to a call center agent. Please hold.', voice=voice_language)
        resp.dial('+18004321000')
    else:
        resp.say('Have a great day!', voice=voice_language)
        resp.hangup()
    print 'Returned TwiML: ' + str(resp)
    return str(resp)


########
########  Use cases
########
@app.route('/option_menu', methods=['POST', 'GET'])
def option_menu():
    delivery_type = request.values.get('DeliveryType', 'SMS')
    incoming_phone_number = request.values.get('From', '')
    msg_body = request.values.get('Body', '')
    msg_body = (msg_body.lstrip()).rstrip()
    print "Incoming Phone Number: " + incoming_phone_number
    reply_msg = ''
    # Reply back to sender
    resp = twiml.Response()
    if delivery_type == 'SMS':
        print "SMS Body: " + msg_body
        if msg_body.upper() in ('BAL', 'DUE', 'STATUS'):
            reply_msg = get_options_menu(msg_body.upper())
            reply_msg = reply_msg + '\nPlease call your agent if you have additional questions.'
        else:
            reply_msg = 'Thanks for your message.  Valid values: Bal, Due, Status.  Please try again.'
        resp.message(reply_msg)
    elif delivery_type == 'VOICE':
        resp.play('/media/greeting.mp3')
        if incoming_phone_number in ('+12018874712'):
            resp.say('Connecting you directly to your support contact.  Please hold')
            resp.dial('+18004321000')
        else:
            gather_action_url = '/option_menu_gather?'
            with resp.gather(numDigits=1, action=gather_action_url, method="POST") as g:
                g.say('Please make a selection. ', voice=voice_language)
                g.say('Press 1 for Account Balance', voice=voice_language)
                g.say('Press 2 for Payment Due Date', voice=voice_language)
                g.say('Press 3 for Account Approval Status', voice=voice_language)
                g.say('Press 0 to speak with a customer support agent', voice=voice_language)

    print 'Returned TwiML: ' + str(resp)
    # Reply back to customer
    return str(resp)

@app.route('/option_menu_gather', methods=['POST', 'GET'])
def lunch_menu_gather():
    digits = request.values.get('Digits', '')
    print 'Digit Pressed: ' + digits
    resp = twiml.Response()
    if digits == '1':
        reply_msg = get_options_menu('BAL')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '2':
        reply_msg = get_options_menu('DUE')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '3':
        reply_msg = get_options_menu('STATUS')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '0':
        resp.say('Connecting you to call center', voice=voice_language)
        resp.dial(number='+18887108756')
    else:
        resp.say('Unknown Selection. Please try again.', voice=voice_language)
        resp.redirect('/option_menu?DeliveryType=VOICE')
    print 'Returned TwiML: ' + str(resp)
    return str(resp)


@app.route('/', methods=['POST', 'GET'])
def hello():
    print request.headers
    return 'Hello World!'

@app.route('/<customer>', methods=['POST', 'GET'])
def hello_customer(customer):
    if request.method == 'GET':
        print request.headers
        customer_name = customer
        print 'Hello ' + customer_name
        return app.send_static_file('index.html')
    elif request.method == 'POST':
        customer_name = customer
        print 'Hello ' + customer_name
        f = request.json
        message_body = f['body']
        send_outbound_alerts(customer_name, message_body)
        return 'OK'


if __name__ == '__main__':
    app.run(debug=True)