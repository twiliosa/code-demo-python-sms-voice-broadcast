import os
import urllib
import requests
import config
import json
from twilio import twiml, TwilioRestException
from twilio.rest import TwilioRestClient

from firebase import firebase

from flask import Flask, redirect, url_for
from flask import request, json

app = Flask(__name__, static_url_path='')

#From Numbers:
FROM_NUMBER_UK = '+441323702021'
FROM_NUMBER_US_573 = '+15737725629'
FROM_NUMBER_US_978 = '+19787672498'
FROM_NUMBER_US_469 = '+14695184100'
FROM_SHORT_CODE = '894546'

from_number_voice = FROM_NUMBER_US_469
from_number_sms = FROM_NUMBER_US_469
to_store_invited_info = FROM_NUMBER_US_573
voice_language = 'en-US'

# Customer specific global values
forward_number = ''
greeting_mp3 = ''


# Dictionary of options
options_menu_list = {'BAL': 'Your current balance:\n$100\nHave a great day!',
            'DUE': 'Your payment is due on:\nApril 15, 2014\nHave a great day!',
            'STATUS': 'Your current loan approval status:\nUnder Final Review\nHave a great day!'
            }

def get_options_menu(option):
    try:
        option_menu = options_menu_list[option]
    except KeyError:
        option_menu = "Not Available"
    return option_menu


def update_participant_firebase_status(customer_name, user_name, notification_type, phone_number, status):
    fb = firebase.FirebaseApplication('https://ab-alerts-twilio.firebaseio.com', None)
    participant_path = '/' + customer_name + '/users/' + user_name + '/participants/' + notification_type + '/' + urllib.quote_plus(phone_number)
    print participant_path
    fb.patch(participant_path, {'status':status})


def get_customer_firebase_assets(customer_name):
    global forward_number
    global greeting_mp3
    #customer_name = request.values.get('customer_name')
    customer_assets = {}
    fb = firebase.FirebaseApplication('https://ab-alerts-twilio.firebaseio.com', None)
    fb_path = '/' + customer_name + '/assets/'
    greeting_mp3 = fb.get(fb_path, 'greeting')
    forward_number = fb.get(fb_path, 'forward_number')
    return 'ok'

# Send outbound Alerts
def send_outbound_alerts(customer_name, user_name, message_body):
    # f = request.json
    # message = f['body']
    message = message_body
    print 'Message Text: ' + message
    # Get Twilio client object
    client = TwilioRestClient(config.ACCOUNT_SID, config.AUTH_TOKEN)
    sms_participants = {}
    voice_participants = {}
    fb = firebase.FirebaseApplication('https://ab-alerts-twilio.firebaseio.com', None)
    fb_path = '/' + customer_name + '/users/' + user_name + '/participants'
    sms_participants = fb.get(fb_path, 'sms')
    voice_participants = fb.get(fb_path, 'voice')
    participant = {}
    # Process SMS Alerts outbound
    try:
        for key in sms_participants.keys():
            participant['phone'] = key
            print key
            for k, v in sms_participants[key].iteritems():
                participant[k] = v
                print k + ' => ' + v
            # Now send the SMS
            message_body = 'Hello ' + participant['name'] + ':\n' + message
            print 'Outbound Message Body: ' + message_body
            params = {'customer': customer_name, 'user': user_name}
            sms_status_callback_url = 'https://' + request.host + '/sms_callback?' + urllib.urlencode(params)
            sms = client.messages.create(to=participant['phone'], from_=from_number_sms, body =message_body, status_callback =sms_status_callback_url)
            print "Message Sid: " + sms.sid
            participant_path = fb_path + '/sms/' + urllib.quote_plus(participant['phone'])
            print participant_path
            update_participant_firebase_status(customer_name, user_name, 'sms', participant['phone'], sms.status)
            # fb.patch(participant_path, {'status':sms.status})
    except:
        pass
    participant = {}
    # Process Voice Alerts outbound
    try:
        for key in voice_participants.keys():
            participant['phone'] = key
            for k, v in voice_participants[key].iteritems():
                participant[k] = v
                print k + ' => ' + v
            # Now send the Voice call
            message_body = 'Hello ' + participant['name'] + ':\n' + message
            print 'Outbound Message Body: ' + message_body
            params = {'customer': customer_name, 'user': user_name, 'voicemessage' : message_body}
            call_url = 'https://' + request.host + '/outbound_ivr?' + urllib.urlencode(params)
            # print 'Call URL: ' + call_url
            voice_status_callback_url = 'https://' + request.host + '/voice_callback?' + urllib.urlencode(params)
            call = client.calls.create(from_=from_number_voice, to=participant['phone'], url=call_url, status_callback =voice_status_callback_url)
            update_participant_firebase_status(customer_name, user_name, 'voice', participant['phone'], 'calling')
    except:
        pass
    return 'OK'


# Twilio SMS Sent callback
@app.route('/sms_callback', methods=['POST', 'GET'])
def sms_callback():
    to_number = request.values.get('To')
    status = request.values.get('MessageStatus', 'NA')
    customer_name = request.values.get('customer')
    user_name = request.values.get('user')
    print 'Message To: ' + to_number
    print 'Message Status: ' + status
    print 'Customer Name: ' + customer_name
    print 'User Name: ' + user_name
    # Update status on Firebase
    update_participant_firebase_status(customer_name, user_name, 'sms', to_number, status)
    resp = twiml.Response()
    return str(resp)


# Twilio Voice Sent callback
@app.route('/voice_callback', methods=['POST', 'GET'])
def voice_callback():
    to_number = request.values.get('To')
    status = request.values.get('CallStatus', 'NA')
    customer_name = request.values.get('customer')
    user_name = request.values.get('user')
    print 'Call To: ' + to_number
    print 'Call Status: ' + status
    print 'Customer Name: ' + customer_name
    print 'User Name: ' + user_name
    # Update status on Firebase
    update_participant_firebase_status(customer_name, user_name, 'voice', to_number, status)
    resp = twiml.Response()
    return str(resp)


# Broadcast repair info to customers
@app.route('/outbound_ivr', methods=['POST', 'GET'])
def outbound_ivr():
    to_number = request.values.get('To')
    voice_message = request.values.get('voicemessage', 'Hello')
    customer_name = request.values.get('customer')
    user_name = request.values.get('user')
    print voice_message
    resp = twiml.Response()
    params = {'customer': customer_name, 'user': user_name, 'voicemessage' : voice_message}
    gather_action_url = 'https://' + request.host + '/outbound_ivr_process_gather?' + urllib.urlencode(params)
    # print gather_action_url
    with resp.gather(numDigits=1, action=gather_action_url, method="POST") as g:
        g.say('Press 1 to listen to your message', voice=voice_language)
        g.say('Press 2 to speak to an agent', voice=voice_language)
        g.say('Press 3 to up lfit your mood', voice=voice_language)
        g.say('Press 0 to hangup', voice=voice_language)
    update_participant_firebase_status(customer_name, user_name, 'voice', to_number, 'answered')
    # Reply back with TwiML
    print 'Returned TwiML: ' + str(resp)
    return str(resp)

@app.route('/outbound_ivr_process_gather', methods=['POST', 'GET'])
def outbound_ivr_process_gather():
    voice_message = request.values.get('voicemessage', 'Hello')
    digits = request.values.get('Digits', '')
    customer_name = request.values.get('customer')
    print 'Digit Pressed: ' + digits
    resp = twiml.Response()
    if digits == '1':
        resp.say(voice_message, voice=voice_language)
    elif digits == '2':
        resp.say('Connecting to a call center agent. Please hold.', voice=voice_language)
        if forward_number:
            resp.dial (forward_number)
        else:
            resp.dial('+12242053900')
    elif digits == '3':
        resp.play(greeting_mp3)
    else:
        resp.say('Have a great day!', voice=voice_language)
        resp.hangup()
    print 'Returned TwiML: ' + str(resp)
    return str(resp)


########
########  Use cases
########
@app.route('/option_menu', methods=['POST', 'GET'])
def option_menu():
    delivery_type = request.values.get('DeliveryType', 'SMS')
    incoming_phone_number = request.values.get('From', '')
    msg_body = request.values.get('Body', '')
    msg_body = (msg_body.lstrip()).rstrip()
    print "Incoming Phone Number: " + incoming_phone_number
    reply_msg = ''
    # Reply back to sender
    resp = twiml.Response()
    if delivery_type == 'SMS':
        print "SMS Body: " + msg_body
        if msg_body.upper() in ('BAL', 'DUE', 'STATUS'):
            reply_msg = get_options_menu(msg_body.upper())
            reply_msg = reply_msg + '\nPlease call your agent if you have additional questions.'
        else:
            reply_msg = 'Thanks for your message.  Valid values: Bal, Due, Status.  Please try again.'
        resp.message(reply_msg)
    elif delivery_type == 'VOICE':
        resp.play('/media/greeting.mp3')
        if incoming_phone_number in ('+12018874712'):
            resp.say('Connecting you directly to your support contact.  Please hold')
            resp.dial('+18004321000')
        else:
            gather_action_url = '/option_menu_gather?'
            with resp.gather(numDigits=1, action=gather_action_url, method="POST") as g:
                g.say('Please make a selection. ', voice=voice_language)
                g.say('Press 1 for Account Balance', voice=voice_language)
                g.say('Press 2 for Payment Due Date', voice=voice_language)
                g.say('Press 3 for Account Approval Status', voice=voice_language)
                g.say('Press 0 to speak with a customer support agent', voice=voice_language)

    print 'Returned TwiML: ' + str(resp)
    # Reply back to customer
    return str(resp)

@app.route('/option_menu_gather', methods=['POST', 'GET'])
def option_menu_gather():
    digits = request.values.get('Digits', '')
    print 'Digit Pressed: ' + digits
    resp = twiml.Response()
    if digits == '1':
        reply_msg = get_options_menu('BAL')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '2':
        reply_msg = get_options_menu('DUE')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '3':
        reply_msg = get_options_menu('STATUS')
        resp.sms(reply_msg)
        resp.say(reply_msg, voice=voice_language)
    elif digits == '0':
        resp.say('Connecting you to call center', voice=voice_language)
        resp.dial(number='+18887108756')
    else:
        resp.say('Unknown Selection. Please try again.', voice=voice_language)
        resp.redirect('/option_menu?DeliveryType=VOICE')
    print 'Returned TwiML: ' + str(resp)
    return str(resp)


@app.route('/send_invite/mms', methods=['POST', 'GET'])
def send_invite():
    f = request.json
    to_number = f['mobile_number']
    first_name = f['first_name']
    last_name = f['last_name']
    company = f['company']

    # Save Customer info
    try:
        # Get Twilio client object
        client = TwilioRestClient(config.PARENT_ACCOUNT_SID, config.PARENT_AUTH_TOKEN)
        message_body = 'FirstName: ' + first_name + ' LastName: ' + last_name + ' Company: ' + company + ' Mobile Phone: ' + to_number
        print message_body
        message = client.messages.create(to=to_store_invited_info, from_=from_number_sms, body =message_body)
        print "Save info Message Sid: " + message.sid
    except:
        print "Unable to Save Customer Info"
        pass

    # Send Invite to Customer
    try:
        client = TwilioRestClient(config.SHORTCODE_ACCOUNT_SID, config.SHORTCODE_ACCOUNT_TOKEN)
        media_url = 'http://status.twilio.com/images/logo.png'
        # message_body = 'Twilio Dinner Details:\nTuesday, March 18 , 2014\n7:30 pm - 10:30 pm\n\nCharley\'s Steak House\n2901 Parkway Blvd\nKissimmee, FL\n\nFree shuttle departs The Gaylord front lobby at 7:15 pm'
        message_body = 'https://www.twilio.com/try-twilio'
        # Get Twilio client object
        message = client.messages.create(to=to_number, from_=FROM_SHORT_CODE, body =message_body, media_url=media_url)
        print "Message Sid: " + message.sid
        status = 'Invite MMS sent'
    except:
        status = 'Invite MMS failed'
        pass

    return status


@app.route('/', methods=['POST', 'GET'])
def hello():
    print request.headers
    return 'Hello World!'

@app.route('/assets/<type>/<filename>', methods=['GET'])
def serve_static_files(type, filename):
    filename = type + '/' + filename
    return app.send_static_file(filename)

@app.route('/c/<customer>', methods=['GET'])
def redirect_to(customer):
    return redirect('/c/' + customer + '/guest')

@app.route('/c/<customer>/<user>', methods=['POST', 'GET'])
def hello_customer(customer, user):
    if request.method == 'GET':
        # print request.headers
        customer_name = customer
        user_name = user
        print 'Hello ' + customer_name + ' User ' + user
        return app.send_static_file('index.html')
    elif request.method == 'POST':
        customer_name = customer
        user_name = user
        print 'Customer Name: ' + customer_name
        get_customer_firebase_assets(customer_name)
        print 'Customer Greeting: ' + greeting_mp3
        print 'Customer Forwarding Number: ' + forward_number
        f = request.json
        message_body = f['body']
        send_outbound_alerts(customer_name, user_name, message_body)
        return 'OK'

@app.route('/invite', methods=['GET'])
def show_invite():
    return app.send_static_file('index.html')

if __name__ == '__main__':
    app.run(debug=True)