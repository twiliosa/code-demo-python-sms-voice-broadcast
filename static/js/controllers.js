'use strict';

twilioBroadcast.controller('mainController',
  ['$scope', '$http', '$routeParams', '$location','$firebase', '$timeout',
	function ($scope, $http, $routeParams, $location, $firebase, $timeout)
{
  var firebaseURL = "https://ab-alerts-twilio.firebaseio.com/";
  // {
  //   "barclays" : {
  //     "assets" : {
  //       "logo" : "http://globalaccessibilitynews.com/files/2012/11/Barclays-logo.jpg",
  //       "messages" : ["hi", "test"]
  //     },
  //     "participants" : {
  //       "voice" : {
  //         "+16507439658" : {
  //           "name" : "Ameer",
  //           "status" : "completed"
  //         }
  //       },
  //       "sms" : {
  //         "+14159387868" : {
  //           "name" : "Ameer GV",
  //           "status" : "sent"
  //         }
  //       }
  //     }
  //   }
  // }

  // Add FIREBASE functionality.
  // Name, phone_number, notification_type, status
  var demo = $scope.demo = {
    recipients: [],                   // an array of recipient objects.
    company: $routeParams.company,    // name of the company
    logo: '',                         // absolute path to logo
    message: '',                      // the selected message that will display
    messages: [],                     // an array of messages
    user: $routeParams.user,          // This is the SE that is demoing the app.
    saving: false,
    submitting: false,
    standardizePhoneNumbers: function standardizePhoneNumbers () {
      for (var i = 0; i < this.recipients.length; i++) {
        if (this.recipients[i].phone_number.length > 0)
          this.recipients[i].phone_number = '+' + this.recipients[i]
            .phone_number.replace(/\D/g,'');
      }
    },
    saveRecipients: function saveRecipients () {
      this.saving = true;

      this.standardizePhoneNumbers();

      var path = this.company + '/users/' + this.user + '/participants/';

      // Remove all previous participants first.
      this.firebase.$child(path).$remove();

      // Go through array of recipients and add to Firebase.
      for (var i = 0; i < this.recipients.length; i++) {
        // If all of the fields were filled out, then add it to Firebase.
        if (this.recipients[i].name.length > 0 &&
          this.recipients[i].phone_number.length > 0) {
          this.firebase.$child(path + this.recipients[i].notification_type +
            "/" + this.recipients[i].phone_number + "/name")
            .$set(this.recipients[i].name);
          this.firebase.$child(path + this.recipients[i].notification_type +
            "/" + this.recipients[i].phone_number + "/status")
            .$set(this.recipients[i].status);
        }
      }

      // Show a message that says 'Saved!'
      this.saving = false;
      this.saved = true;
      $timeout(function() {
        demo.saved = false;
      }, 3000);
    },
    addRecipient: function addRecipient (rec) {
      rec.can_delete = true;

      this.recipients.push({
        name: '',
        phone_number: '',
        notification_type: 'sms',
        status: 'uncontacted',
        can_delete: false
      });
    },
    removeRecipient: function removeRecipient (index) {
      this.recipients.splice(index, 1);
    },
    removeAllRecipients: function removeAllRecipients () {
      // Empty Firebase information.
      this.firebase.$child(this.company + '/users/' + this.user + '/participants')
        .$remove();

      // Empty local array.
      this.recipients.length = 0;

      // Add one empty person to make it look normal.
      this.recipients.push({
        name: '',
        phone_number: '',
        notification_type: 'sms',
        status: 'uncontacted',
        can_delete: false
      });
    },
    countChar: function countChar() {
      // Count characters for the text field.
      // $('#charNum').text($('#messageBody').val().length);
      $('#charNum').text(this.message.length);
    },
    submit: function submit () {
      this.submitting = true;
      // Submit button will POST message to server.
      var msgObj = {};
      msgObj.body = $('#messageBody').val();
      $http.post('/c/' + this.company + '/' + this.user, msgObj)
      .success(function(data) {
        // Success!
        console.log('Success: ' + data);
        demo.submitting = false;
      })
      .error(function(data) {
        // Error.
        console.log('Error: ' + data);
        demo.submitting = false;
      });
    }
  };

  demo.firebase = $firebase(new Firebase(firebaseURL));

  demo.firebase.$on("loaded", function (data) {
    // Set messages and logo.
    demo.logo = data[demo.company].assets.logo;

    for (var property in data[demo.company].assets.messages) {
      demo.messages.push(data[demo.company].assets.messages[property]);
    }

    demo.message = demo.messages[0];

    demo.countChar();

    // Retrieve participants but under /user/ namespace.
    // Check to see if it exists first.
    if (data[demo.company].users[demo.user]) {
      for (property in data[demo.company].users[demo.user].participants) {
        // property: 'sms', 'voice'
        for (var p in data[demo.company].users[demo.user].participants[property]) {
          // p: '+12409887757'
          var participants = data[demo.company].users[demo.user].participants[property];
          if (property == 'sms' || property == 'voice') {
            participants[p].status = 'uncontacted';
            var newRecipient = {
              name: participants[p].name,
              notification_type: property,
              phone_number: p,
              status: participants[p].status,
              can_delete: true
            };
            demo.recipients.push(newRecipient);
          }
        }
      }
    }

    // Add a blank one.
    demo.recipients.push({
      name: '',
      phone_number: '',
      notification_type: 'sms',
      status: 'uncontacted',
      can_delete: false
    });
  });

  demo.firebase.$on("child_changed", function (data) {
    // console.log(data);

    if (data.snapshot.value.users[demo.user]) {
      var children = data.snapshot.value.users[demo.user].participants;
      var arr = [];

      for (var prop in children) {
        var numbers = Object.keys(children[prop]);
        for (var i = 0; i < numbers.length; i++) {
          arr.push({
            type: prop,
            phone_number: numbers[i]
          });
        }
      }

      // If the number and the type match, then change the status.
      for (var k = 0; k < demo.recipients.length; k++) {
        for (var j = 0; j < arr.length; j++) {
          if (demo.recipients[k].phone_number == arr[j].phone_number &&
            demo.recipients[k].notification_type == arr[j].type) {
            demo.recipients[k].status = children[arr[j].type][arr[j].phone_number].status;
          }
        }
      }
    }
  });
}]);

twilioBroadcast.controller('adminController',
  ['$scope', '$http', '$routeParams', '$location','$firebase', '$timeout',
  function ($scope, $http, $routeParams, $location, $firebase, $timeout)
{
  var firebaseURL = "https://ab-alerts-twilio.firebaseio.com/";

  var rootUrl = $location.$$absUrl;
  rootUrl = rootUrl.replace($location.$$path, '/');

  var company = $scope.company = {
    name: '',
    logo: '',
    messages: [], // {body: '', can_delete: true}, ..
    saved: false,
    url: rootUrl,
    addMessage: function addMessage (msg) {
      // console.log(msg);
      msg.can_delete = true;

      this.messages.push({
        body: '',
        can_delete: false
      });
    },
    removeMessage: function removeMessage (index) {
      this.messages.splice(index, 1);
    },
    save: function save () {
      this.name = this.name.toLowerCase();
      
      // this.saving = true;
      if (this.name.length > 0) {
        this.firebase.$child(this.name + '/assets/logo').$set(this.logo);
        for (var i = 0; i < this.messages.length; i++) {
          if (this.messages[i].body.length > 0)
            this.firebase.$child(this.name + '/assets/messages/' + i)
              .$set(this.messages[i].body);
        }

        this.url = this.url + this.name;

        // Show a message that says 'Saved!'
        this.saved = true;
      }
    }
  };

  company.messages.push({
    body: '',
    can_delete: false
  });

  company.firebase = $firebase(new Firebase(firebaseURL));

}]);

twilioBroadcast.controller('inviteController',
  ['$scope', '$http', '$routeParams', '$location','$firebase', '$timeout',
  function ($scope, $http, $routeParams, $location, $firebase, $timeout)
{
  // Initialize variables.
  var invite = $scope.invite = {
    firstName: '',
    lastName: '',
    company: '',
    // title: '',
    number: '',
    submitting: false,
    submit: function submit () {
      this.submitting = true;
      var load = {
        mobile_number: this.number,
        first_name: this.firstName,
        last_name: this.lastName,
        company: this.company
      };
      $http.post('/send_invite/mms', load)
      .success(function (data) {
        console.log('Success: ');
        console.log(data);
        invite.submitting = false;
      })
      .error(function (data) {
        console.log('Error: ');
        console.log(data);
        invite.submitting = false;
      });
    }
  };

}]);