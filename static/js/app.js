'use strict';

// Declare app level module which depends on services, etc. ====================
var twilioBroadcast = angular.module('twilioBroadcast',
	["ngRoute", "firebase"]);