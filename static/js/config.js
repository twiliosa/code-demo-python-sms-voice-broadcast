twilioBroadcast.config( ['$routeProvider', '$locationProvider',
function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider.
  when('/admin', {
  	templateUrl: '/pages/admin.html',
  	controller: 'adminController'
  }).
  when('/invite', {
    templateUrl: '/pages/invite.html',
    controller: 'inviteController'
  }).
  when('/c/:company/:user', {
    templateUrl: '/pages/page.html',
    controller: 'mainController'
  }).
  otherwise({
    redirectTo: '/'
  });
}]);